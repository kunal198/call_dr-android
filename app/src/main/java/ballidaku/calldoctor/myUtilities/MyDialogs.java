package ballidaku.calldoctor.myUtilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.mainScreens.activities.DetailsActivity;

/**
 * Created by brst-pc93 on 7/14/17.
 */

public class MyDialogs
{
    public Dialog dialog;


    private static MyDialogs  instance = new MyDialogs();

    public static MyDialogs getInstance()
    {
        return instance;
    }


    public void addFees(final Context context)
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_fees);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();


        Button buttonSubmit = (Button) dialog.findViewById(R.id.buttonSubmit);

        buttonSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonMethods.getInstance().hideKeyBoard((DetailsActivity)context);
                dialog.dismiss();

            }
        });

        /*

        ImageView imgvCenter = (ImageView) dialog.findViewById(R.id.imgvCenter);

        TextView txtv_btn = (TextView) dialog.findViewById(R.id.txtv_btn);


        if (fromWhere.equals("DailyInspiration"))
        {
            txtv_title.setText("DAILY INSPIRATION");
            txtv_btn.setText("Submit 10 Pts");

        }

        myUtil.showImageWithPicasso(context, imgvCenter, image);


        CardView cardViewBtn = (CardView) dialog.findViewById(R.id.cardViewBtn);

        cardViewBtn.setOnClickListener(onClickListener);


        if (count == 0)
        {
            cardViewBtn.setVisibility(View.VISIBLE);
        }
        else
        {
            cardViewBtn.setVisibility(View.GONE);
        }*/


    }


    public void commonDialog(final Context context, final Activity activity, String title, String buttonName, int where)
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_common);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();


        TextView textViewTitle = (TextView) dialog.findViewById(R.id.textViewTitle);
        textViewTitle.setText(title);

        EditText editTextOldPassword = (EditText) dialog.findViewById(R.id.editTextOldPassword);
        EditText editTextNewPassword = (EditText) dialog.findViewById(R.id.editTextNewPassword);
        EditText editTextConfirmPassword = (EditText) dialog.findViewById(R.id.editTextConfirmPassword);
        EditText editTextEmail = (EditText) dialog.findViewById(R.id.editTextEmail);



        if(where == 1)
        {
            editTextOldPassword.setVisibility(View.GONE);
            editTextNewPassword.setVisibility(View.GONE);
            editTextConfirmPassword.setVisibility(View.GONE);

            editTextEmail.setVisibility(View.VISIBLE);
        }
        else
        {
            editTextOldPassword.setVisibility(View.VISIBLE);
            editTextNewPassword.setVisibility(View.VISIBLE);
            editTextConfirmPassword.setVisibility(View.VISIBLE);

            editTextEmail.setVisibility(View.GONE);
        }


        Button buttonSubmit = (Button) dialog.findViewById(R.id.buttonSubmit);
        buttonSubmit.setText(buttonName);

        buttonSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonMethods.getInstance().hideKeyBoard(activity);

                dialog.dismiss();

            }
        });

    }



    AlertDialog alertDialog;

    public void showExitDialog(Context context, DialogInterface.OnClickListener onClickListener)
    {
        alertDialog = new AlertDialog.Builder(context)
                  .setTitle("Exit Confirmation")
                  .setMessage("Are you sure, you want to exit?")
                  .setPositiveButton("EXIT", onClickListener)
                  .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
                  {
                      public void onClick(DialogInterface dialog, int which)
                      {
                          dialog.dismiss();
                      }
                  })
                  .setIcon(R.mipmap.ic_alert)
                  .show();
    }
}
