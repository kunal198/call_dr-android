package ballidaku.calldoctor.myUtilities;

/**
 * Created by brst-pc93 on 7/12/17.
 */

public class MyConstants
{

    public static  String WHO="provider";

    public static final String PROVIDER="provider";
    public static final String PATIENT="patient";

    public static final String SEARCH="search";
    public static final String MESSAGES="messages";
    public static final String BOOKINGS="bookings";
    public static final String MY_PROFILE="my_profile";

    public static final String HEADER="header";
    public static final String ITEM="item";

    public static final String BOOK="book";
    public static final String REQUESTED="requested";
    public static final String ACCEPTED="accepted";

}
