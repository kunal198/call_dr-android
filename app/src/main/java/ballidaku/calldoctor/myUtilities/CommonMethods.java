package ballidaku.calldoctor.myUtilities;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import ballidaku.calldoctor.R;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class CommonMethods
{

    public static CommonMethods instance = new CommonMethods();

    public static CommonMethods getInstance()
    {
        return instance;
    }


    public void changeFragment(FragmentActivity activity, Fragment fragment,boolean haveToAddBackStack)
    {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        if(haveToAddBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public String parseDateToddMMyyyy(String time)
    {

        String inputPattern = "yyyy-MM-dd HH:mm";
        String outputPattern = "dd MMM, yyyy @ h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        String str = null;
        try
        {
            str = outputFormat.format(inputFormat.parse(time));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return str;
    }

    public String parseDateToddMMyyyy2(String date)
    {

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);

        String str = null;
        try
        {
            str = outputFormat.format(inputFormat.parse(date));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return str;
    }



    public void hideKeyBoard(Activity activity)
    {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }




}
