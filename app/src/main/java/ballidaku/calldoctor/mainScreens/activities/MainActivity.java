package ballidaku.calldoctor.mainScreens.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.frontScreens.LoginActivity;
import ballidaku.calldoctor.mainScreens.fragments.Chat;
import ballidaku.calldoctor.mainScreens.fragments.Contact_Not_Working;
import ballidaku.calldoctor.mainScreens.fragments.History;
import ballidaku.calldoctor.mainScreens.fragments.Messages;
import ballidaku.calldoctor.mainScreens.fragments.MyBookings;
import ballidaku.calldoctor.mainScreens.fragments.OtherProfile;
import ballidaku.calldoctor.mainScreens.fragments.Profile;
import ballidaku.calldoctor.mainScreens.fragments.Search;
import ballidaku.calldoctor.mainScreens.fragments.SelectDoctor;
import ballidaku.calldoctor.myUtilities.CommonMethods;
import ballidaku.calldoctor.myUtilities.MyConstants;
import ballidaku.calldoctor.myUtilities.MyDialogs;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    String TAG = MainActivity.class.getSimpleName();
    Context context;

    public TextView textViewTitle;
    public TextView textViewSubTitle;

    LinearLayout linearLayoutMain;

    public LinearLayout linearLayoutTabs;

    View viewSearch;

    LinearLayout linearLayoutSearch;
    LinearLayout linearLayoutMessages;
    LinearLayout linearLayoutBookings;
    LinearLayout linearLayoutMyProfile;

    String lastView = "";

    Fragment fragment = null;

    PopupWindow popupWindow;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        context = this;

        setUpViews();
    }

    public Toolbar toolbar;

    private void setUpViews()
    {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);


        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewSubTitle = (TextView) findViewById(R.id.textViewSubTitle);


        linearLayoutMain = (LinearLayout) findViewById(R.id.linearLayoutMain);

        linearLayoutTabs = (LinearLayout) findViewById(R.id.linearLayoutTabs);

        viewSearch = findViewById(R.id.viewSearch);

        linearLayoutSearch = (LinearLayout) findViewById(R.id.linearLayoutSearch);
        linearLayoutMessages = (LinearLayout) findViewById(R.id.linearLayoutMessages);
        linearLayoutBookings = (LinearLayout) findViewById(R.id.linearLayoutBookings);
        linearLayoutMyProfile = (LinearLayout) findViewById(R.id.linearLayoutMyProfile);

        linearLayoutSearch.setOnClickListener(this);
        linearLayoutMessages.setOnClickListener(this);
        linearLayoutBookings.setOnClickListener(this);
        linearLayoutMyProfile.setOnClickListener(this);


        if (MyConstants.WHO.equals(MyConstants.PROVIDER))
        {
            linearLayoutSearch.setVisibility(View.GONE);
            viewSearch.setVisibility(View.GONE);

            linearLayoutMessages.performClick();
        }
        else
        {
            linearLayoutSearch.performClick();
        }

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.linearLayoutSearch:
                removeFragments();
                changeView(lastView = MyConstants.SEARCH);
                changeFragment(0, false);
                // textViewSubTitle.setVisibility(View.GONE);

                break;

            case R.id.linearLayoutMessages:
                removeFragments();
                changeView(lastView = MyConstants.MESSAGES);
                changeFragment(1, false);
                // textViewSubTitle.setVisibility(View.GONE);

                break;

            case R.id.linearLayoutBookings:
                removeFragments();
                changeView(lastView = MyConstants.BOOKINGS);
                changeFragment(2, false);
                //textViewSubTitle.setVisibility(View.GONE);

                break;


            case R.id.linearLayoutMyProfile:
                removeFragments();
                changeView(lastView = MyConstants.MY_PROFILE);
                changeFragment(3, false);
                //textViewSubTitle.setVisibility(View.VISIBLE);

                break;

            case R.id.textViewHistory:
                removeFragments();
                changeFragment(4, true);
                popupWindow.dismiss();
                //textViewSubTitle.setVisibility(View.GONE);
                changeView("None");

                break;

            case R.id.textViewContact:
                removeFragments();
                changeFragment(5, true);
                popupWindow.dismiss();
                //textViewSubTitle.setVisibility(View.GONE);
                changeView("None");

                break;

            case R.id.textViewLogout:

                startActivity(new Intent(context, LoginActivity.class));
                finish();

                break;

        }
    }

    public void removeFragments()
    {
        int count = getSupportFragmentManager().getBackStackEntryCount();
//        Log.e("Count", "" + count);
        if (count != 0)
        {
            while (count != 0)
            {
                getSupportFragmentManager().popBackStackImmediate();
                count = getSupportFragmentManager().getBackStackEntryCount();
//                Log.e("InSide Count", "" + count);
            }
        }
    }


    public void changeView(String where)
    {

        linearLayoutSearch.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
        linearLayoutMessages.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
        linearLayoutBookings.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
        linearLayoutMyProfile.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));


        switch (where)
        {
            case MyConstants.SEARCH:

                linearLayoutSearch.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreenDark));

                break;

            case MyConstants.MESSAGES:

                linearLayoutMessages.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreenDark));

                break;

            case MyConstants.BOOKINGS:

                linearLayoutBookings.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreenDark));

                break;


            case MyConstants.MY_PROFILE:

                linearLayoutMyProfile.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreenDark));

                break;
        }
    }


    public void changeFragment(int position, boolean addToBackStack)
    {
       CommonMethods.getInstance().hideKeyBoard(this);

        switch (position)
        {
            case 0:
                fragment = new Search();

                break;

            case 1:
                fragment = new Messages();
                break;

            case 2:
                fragment = new MyBookings();
                break;


            case 3:
                fragment = new Profile();
                break;

            case 4:
                fragment = new History();


                break;

            case 5:
                //fragment = new Contact_Not_Working();

                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:contact@callDoctor.in"));
                intent.putExtra("android.intent.extra.SUBJECT", "Feedback");
                intent.putExtra("android.intent.extra.TEXT", "Text Here....");
                startActivity(intent);


                break;

            case 6:
                fragment = new Chat();
                break;

            case 7:
                fragment = new SelectDoctor();
                break;

            case 8:
                fragment = new OtherProfile();
                break;


            default:
                break;
        }

        if (fragment != null)
        {
            CommonMethods.getInstance().changeFragment(this, fragment, addToBackStack);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                CommonMethods.getInstance().hideKeyBoard(this);

                Fragment f = getSupportFragmentManager().findFragmentById(R.id.container_body);

                Log.e("ABC", "" + f);

                if (f instanceof Contact_Not_Working || f instanceof History || f instanceof Chat || f instanceof SelectDoctor || f instanceof OtherProfile)
                {
                    onBackPressed();

                }
                else
                {
                    showMenuPopUp();
                }


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void showMenuPopUp()
    {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_menu_popup, null);


        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        applyDim(linearLayoutMain, 0.8f);

        popupWindow.showAtLocation(popupView, Gravity.NO_GRAVITY, dpToPx(50), dpToPx(40));





        popupView.findViewById(R.id.textViewHistory).setOnClickListener(this);
        popupView.findViewById(R.id.textViewContact).setOnClickListener(this);
        popupView.findViewById(R.id.textViewLogout).setOnClickListener(this);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
            @Override
            public void onDismiss()
            {

                popupWindow.dismiss();
                clearDim(linearLayoutMain);
            }
        });


        // popupWindow.showAsDropDown(view, 0, -40);


    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }



    public static void applyDim(@NonNull ViewGroup parent, float dimAmount)
    {
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, parent.getWidth(), parent.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.add(dim);
    }

    public static void clearDim(@NonNull ViewGroup parent)
    {
        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.clear();
    }

    @Override
    public void onBackPressed()
    {
        changeView(lastView);


        int count = getSupportFragmentManager().getBackStackEntryCount();

         Log.e("onBackPressedCount", "" + count);


        if (count == 0)
        {
            MyDialogs.getInstance().showExitDialog(context, onBackPressedClickListener);
        }
        else
        {
            super.onBackPressed();
        }

    }

    DialogInterface.OnClickListener onBackPressedClickListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            finish();
        }
    };

    @Override
    protected void onResume()
    {
        super.onResume();

        linearLayoutMain.getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
    }

    @Override
    public void onPause()
    {
        linearLayoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(mGlobalLayoutListener);

        super.onPause();
    }


    ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener()
    {
        @Override
        public void onGlobalLayout()
        {

            Rect r = new Rect();
            linearLayoutMain.getWindowVisibleDisplayFrame(r);
            int screenHeight = linearLayoutMain.getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            Log.e(TAG, "keypadHeight = " + keypadHeight);

            if (keypadHeight > screenHeight * 0.15)
            { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                linearLayoutTabs.setVisibility(View.GONE);
            }
            else
            {
                // keyboard is closed
                linearLayoutTabs.setVisibility(View.VISIBLE);
            }
        }
    };
}
