package ballidaku.calldoctor.mainScreens.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.myUtilities.CommonMethods;
import ballidaku.calldoctor.myUtilities.MyDialogs;

/**
 * Created by brst-pc93 on 7/14/17.
 */

public class DetailsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
          GoogleApiClient.OnConnectionFailedListener, View.OnClickListener
{

    String TAG = DetailsActivity.class.getSimpleName();

    Context context;
    DetailsActivity detailsActivity;

    public TextView textViewTitle;
    public TextView textViewSubTitle;


    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;

    SupportMapFragment mapFragment;

    Button buttonConfirm;
    Button buttonComplete;
    LinearLayout linearlayoutCallMessage;


    public DetailsActivity()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        detailsActivity=this;
        context = this;

        setUpViews();
    }


    private void setUpViews()
    {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);


        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewSubTitle = (TextView) findViewById(R.id.textViewSubTitle);

        textViewTitle.setText("Details");
        textViewSubTitle.setVisibility(View.GONE);


        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        (buttonConfirm = (Button) findViewById(R.id.buttonConfirm)).setOnClickListener(this);
        (buttonComplete = (Button) findViewById(R.id.buttonComplete)).setOnClickListener(this);

        linearlayoutCallMessage = (LinearLayout) findViewById(R.id.linearlayoutCallMessage);


        buttonConfirm.setVisibility(View.VISIBLE);
        buttonComplete.setVisibility(View.GONE);
        linearlayoutCallMessage.setVisibility(View.GONE);

    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.buttonConfirm:

                buttonComplete.setVisibility(View.VISIBLE);
                linearlayoutCallMessage.setVisibility(View.VISIBLE);
                break;

            case R.id.buttonComplete:

                MyDialogs.getInstance().addFees(context);

                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else
            {
                //Request Location Permission
                checkLocationPermission();
            }
        }
        else
        {
            // buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }


        // smoothScroll();


        // Add a marker in Sydney, Australia, and move the camera.
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
    }

    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                  .addConnectionCallbacks(this)
                  .addOnConnectionFailedListener(this)
                  .addApi(LocationServices.API)
                  .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        /*mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                  Manifest.permission.ACCESS_FINE_LOCATION)
                  == PackageManager.PERMISSION_GRANTED)
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }*/
    }

    @Override
    public void onConnectionSuspended(int i)
    {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                      Manifest.permission.ACCESS_FINE_LOCATION))
            {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                          .setTitle("Location Permission Needed")
                          .setMessage("This app needs the Location permission, please accept to use location functionality")
                          .setPositiveButton("OK", new DialogInterface.OnClickListener()
                          {
                              @Override
                              public void onClick(DialogInterface dialogInterface, int i)
                              {
                                  //Prompt the user once explanation has been shown
                                  ActivityCompat.requestPermissions(DetailsActivity.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                              }
                          })
                          .create()
                          .show();


            }
            else
            {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                          MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:


                CommonMethods.getInstance().hideKeyBoard(this);
                finish();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}