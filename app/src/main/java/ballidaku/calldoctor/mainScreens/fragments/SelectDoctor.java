package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.adapters.SelectDoctorAdapter;
import ballidaku.calldoctor.dataModels.SelectDoctorModel;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.MyConstants;

/**
 * Created by brst-pc93 on 7/18/17.
 */

public class SelectDoctor  extends Fragment
{
    String TAG = SelectDoctor.class.getSimpleName();

    View view = null;

    Context context;


    RecyclerView recyclerViewSelectDoctor;

    ArrayList<SelectDoctorModel> selectDoctorModelArrayList;

    SelectDoctorAdapter selectDoctorAdapter;

    SelectDoctorModel selectDoctorModel;



    public SelectDoctor()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_select_doctor, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }


    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity)getActivity()).textViewTitle.setText("Select Doctor");
        ((MainActivity)getActivity()).textViewSubTitle.setVisibility(View.GONE);
        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);
    }

    @Override
    public void onDestroy()
    {

        super.onDestroy();

        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

    }

    private void setUpViews()
    {
        recyclerViewSelectDoctor = (RecyclerView) view.findViewById(R.id.recyclerViewSelectDoctor);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewSelectDoctor.getContext(), DividerItemDecoration.VERTICAL);
        recyclerViewSelectDoctor.addItemDecoration(dividerItemDecoration);


        selectDoctorModelArrayList = new ArrayList<>();

        selectDoctorModel = new SelectDoctorModel();
        selectDoctorModel.setUserName("Barry Devito");
        selectDoctorModel.setUserAddress("44 Shirley Ave. West Chicago, IL 60185");
        selectDoctorModel.setUserImage(R.drawable.user_5);
        selectDoctorModel.setStatus(MyConstants.BOOK);
        selectDoctorModelArrayList.add(selectDoctorModel);

        selectDoctorModel = new SelectDoctorModel();
        selectDoctorModel.setUserName("Isa Ladner");
        selectDoctorModel.setUserAddress("514 S. Magnolia St. Orlando, FL 32806");
        selectDoctorModel.setUserImage(R.drawable.user_6);
        selectDoctorModel.setStatus(MyConstants.BOOK);
        selectDoctorModelArrayList.add(selectDoctorModel);

        selectDoctorModel = new SelectDoctorModel();
        selectDoctorModel.setUserName("Andreas Sommer");
        selectDoctorModel.setUserAddress("44 Shirley Ave. West Chicago, IL 60185");
        selectDoctorModel.setUserImage(R.drawable.user_7);
        selectDoctorModel.setStatus(MyConstants.BOOK);
        selectDoctorModelArrayList.add(selectDoctorModel);

        selectDoctorModel = new SelectDoctorModel();
        selectDoctorModel.setUserName("Sierra Pasko");
        selectDoctorModel.setUserAddress("514 S. Magnolia St. Orlando, FL 32806");
        selectDoctorModel.setUserImage(R.drawable.user_8);
        selectDoctorModel.setStatus(MyConstants.BOOK);
        selectDoctorModelArrayList.add(selectDoctorModel);


        selectDoctorAdapter = new SelectDoctorAdapter(context,selectDoctorModelArrayList,itemTouchListener);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewSelectDoctor.setLayoutManager(mLayoutManager);
        recyclerViewSelectDoctor.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelectDoctor.setAdapter(selectDoctorAdapter);


    }


    SelectDoctorAdapter.OnItemTouchListener itemTouchListener = new SelectDoctorAdapter.OnItemTouchListener() {

        @Override
        public void onViewClick(View view, int position)
        {
            ((MainActivity) getActivity()).changeFragment(8, true);
        }

    };








}