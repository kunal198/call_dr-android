package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import java.util.ArrayList;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.adapters.ChatAdapter;
import ballidaku.calldoctor.dataModels.ChatModel;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;

/**
 * Created by brst-pc93 on 7/12/17.
 */

public class Chat extends Fragment
{

    String TAG = Chat.class.getSimpleName();

    View view = null;
    RecyclerView recyclerViewChat;

    EditText editTextMessage;

    ChatAdapter chatAdapter;

    ArrayList<ChatModel> chatModelArrayList;

    ChatModel chatModel;

    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_chat, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        recyclerViewChat = (RecyclerView) view.findViewById(R.id.recyclerViewChat);
        editTextMessage = (EditText) view.findViewById(R.id.editTextMessage);

        chatModelArrayList = new ArrayList<>();

        chatModel = new ChatModel();
        chatModel.setMessage("Hii..We have a appointment tomorrow");
        chatModel.setTime("10:39 am");
        chatModel.setImage(R.drawable.ic_user_1);
        chatModelArrayList.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setMessage("Yes, I remember");
        chatModel.setTime("10:40 am");
        chatModel.setImage(R.drawable.ic_user_2);
        chatModelArrayList.add(chatModel);


        chatAdapter = new ChatAdapter(chatModelArrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewChat.setLayoutManager(mLayoutManager);
        recyclerViewChat.setItemAnimator(new DefaultItemAnimator());
        recyclerViewChat.setAdapter(chatAdapter);




    }




    @Override
    public void onResume()
    {

        super.onResume();


        ((MainActivity) getActivity()).textViewTitle.setText("Dr. Barry Devito");
        ((MainActivity) getActivity()).textViewSubTitle.setVisibility(View.GONE);
        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);
    }




    @Override
    public void onDestroy()
    {

        super.onDestroy();

        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

    }


}
