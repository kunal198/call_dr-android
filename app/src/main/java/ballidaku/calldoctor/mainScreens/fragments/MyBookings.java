package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.adapters.MyBookingsAdapter;
import ballidaku.calldoctor.dataModels.BookingsModel;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class MyBookings extends Fragment
{

    String TAG = Messages.class.getSimpleName();

    View view = null;
    RecyclerView recyclerViewBookings;

    MyBookingsAdapter myBookingsAdapter;

    ArrayList<BookingsModel> bookingsModelArrayList;

    BookingsModel bookingsModel;

    Context context;

    public MyBookings()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_my_bookings, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        recyclerViewBookings = (RecyclerView) view.findViewById(R.id.recyclerViewBookings);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewBookings.getContext(), DividerItemDecoration.VERTICAL);
        recyclerViewBookings.addItemDecoration(dividerItemDecoration);


        bookingsModelArrayList = new ArrayList<>();

        bookingsModel = new BookingsModel();
        bookingsModel.setUserName("Barry Devito");
        bookingsModel.setUserAddress("44 Shirley Ave. West Chicago, IL 60185");
        bookingsModel.setUserImage(R.drawable.user_5);
        bookingsModel.setDate("24 Jul, 2017 @ 5:00 pm");
        bookingsModelArrayList.add(bookingsModel);

        bookingsModel = new BookingsModel();
        bookingsModel.setUserName("Isa Ladner");
        bookingsModel.setUserAddress("514 S. Magnolia St. Orlando, FL 32806");
        bookingsModel.setUserImage(R.drawable.user_6);
        bookingsModel.setDate("24 Jul, 2017 @ 5:00 pm");
        bookingsModelArrayList.add(bookingsModel);

        bookingsModel = new BookingsModel();
        bookingsModel.setUserName("Andreas Sommer");
        bookingsModel.setUserAddress("44 Shirley Ave. West Chicago, IL 60185");
        bookingsModel.setUserImage(R.drawable.user_7);
        bookingsModel.setDate("24 Jul, 2017 @ 5:00 pm");
        bookingsModelArrayList.add(bookingsModel);

        bookingsModel = new BookingsModel();
        bookingsModel.setUserName("Sierra Pasko");
        bookingsModel.setUserAddress("514 S. Magnolia St. Orlando, FL 32806");
        bookingsModel.setUserImage(R.drawable.user_8);
        bookingsModel.setDate("24 Jul, 2017 @ 5:00 pm");
        bookingsModelArrayList.add(bookingsModel);


        myBookingsAdapter = new MyBookingsAdapter(context,bookingsModelArrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewBookings.setLayoutManager(mLayoutManager);
        recyclerViewBookings.setItemAnimator(new DefaultItemAnimator());
        recyclerViewBookings.setAdapter(myBookingsAdapter);


       /* recyclerViewBookings.addOnItemTouchListener(new RecyclerItemClickListener(context, recyclerViewBookings,
                  new RecyclerItemClickListener.OnItemClickListener()
                  {
                      @Override
                      public void onItemClick(View view, int position)
                      {
                          Log.e(TAG,""+position);


                      }

                      @Override
                      public void onLongItemClick(View view, int position)
                      {
                          // do whatever
                      }
                  }));*/
    }

    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity)getActivity()).textViewTitle.setText("My Bookings");
        ((MainActivity)getActivity()).textViewSubTitle.setVisibility(View.GONE);
    }





}
