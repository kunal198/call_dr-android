package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.adapters.HistoryAdapter;
import ballidaku.calldoctor.dataModels.HistoryModel;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.MyConstants;

/**
 * Created by brst-pc93 on 7/14/17.
 */

public class History  extends Fragment
{

    String TAG = History.class.getSimpleName();

    Context context;

    View view = null;

    RecyclerView recyclerViewHistory;

    HistoryAdapter historyAdapter;

    ArrayList<HistoryModel> historyModelArrayList;

    HistoryModel historyModel;



    public History()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_history, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        recyclerViewHistory = (RecyclerView) view.findViewById(R.id.recyclerViewHistory);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewHistory.getContext(), DividerItemDecoration.VERTICAL);
        recyclerViewHistory.addItemDecoration(dividerItemDecoration);


        historyModelArrayList = new ArrayList<>();

        int[] pic ={R.drawable.ic_user_1,R.drawable.ic_user_2,R.drawable.ic_user_3};
        int[] pic1 ={R.drawable.user_5,R.drawable.user_6,R.drawable.user_7};

        String[] s1={"Appointment with Dr. Phillip Marquez","Appointment with Dr. Cruze Derron","Appointment with Dr. Lia Simens"};
        String[] s2={"Appointment with Marquez","Appointment with Derron","Appointment with Simens"};

String[] s;
        int[] p =null;
        if(MyConstants.WHO.equals(MyConstants.PROVIDER))
        {
            p=pic1;
            s=s2;
        }
        else
        {
            p=pic;
            s=s1;

        }

        historyModel = new HistoryModel();
        //historyModel.setType(MyConstants.ITEM);

        historyModel.setAppointMentWith(s[0]);
        historyModel.setDoctorType("Cardiologist");
        historyModel.setAddress("514 S. Magnolia St. Orlando, FL 32806");
        historyModel.setDate("28 July, 2017 @ 5:00 pm");
        historyModel.setUserImage(p[0]);
        historyModelArrayList.add(historyModel);



        historyModel = new HistoryModel();
       // historyModel.setType(MyConstants.ITEM);
        historyModel.setAppointMentWith(s[1]);
        historyModel.setDoctorType("Cardiologist");
        historyModel.setAddress("514 S. Magnolia St. Orlando, FL 32806");
        historyModel.setDate("28 July, 2017 @ 5:00 pm");
        historyModel.setUserImage(p[1]);
        historyModelArrayList.add(historyModel);

        historyModel = new HistoryModel();
       // historyModel.setType(MyConstants.ITEM);
        historyModel.setAppointMentWith(s[2]);
        historyModel.setDoctorType("Cardiologist");
        historyModel.setAddress("514 S. Magnolia St. Orlando, FL 32806");
        historyModel.setDate("28 July, 2017 @ 5:00 pm");
        historyModel.setUserImage(p[2]);
        historyModelArrayList.add(historyModel);


        historyAdapter = new HistoryAdapter(historyModelArrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewHistory.setLayoutManager(mLayoutManager);
        recyclerViewHistory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewHistory.setAdapter(historyAdapter);


       /* recyclerViewBookings.addOnItemTouchListener(new RecyclerItemClickListener(context, recyclerViewBookings,
                  new RecyclerItemClickListener.OnItemClickListener()
                  {
                      @Override
                      public void onItemClick(View view, int position)
                      {
                          Log.e(TAG,""+position);

                          // CommonMethods.getInstance().changeFragment(getActivity(),new Chat(),true);
                      }

                      @Override
                      public void onLongItemClick(View view, int position)
                      {
                          // do whatever
                      }
                  }));*/
    }

    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity)getActivity()).textViewTitle.setText("History");
        ((MainActivity)getActivity()).textViewSubTitle.setVisibility(View.GONE);
        ((MainActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        ((MainActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

    }



}