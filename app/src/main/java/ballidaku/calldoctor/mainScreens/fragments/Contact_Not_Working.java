package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class Contact_Not_Working extends Fragment
{

    String TAG = Contact_Not_Working.class.getSimpleName();

    Context context;

    View view = null;

    public Contact_Not_Working()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_contact, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {

    }

    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity)getActivity()).textViewTitle.setText("Contact_Not_Working");
        ((MainActivity)getActivity()).textViewSubTitle.setVisibility(View.GONE);
        ((MainActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        ((MainActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

    }
}