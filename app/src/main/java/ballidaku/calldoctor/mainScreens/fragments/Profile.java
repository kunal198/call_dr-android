package ballidaku.calldoctor.mainScreens.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Calendar;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.CommonMethods;
import ballidaku.calldoctor.myUtilities.MyConstants;
import ballidaku.calldoctor.myUtilities.MyDialogs;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class Profile extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener
{

    String TAG = Profile.class.getSimpleName();

    Context context;

    View view = null;

    RatingBar ratingBar;
    TextView textViewRating;

    LinearLayout linearLayoutProvider;
    LinearLayout linearLayoutLicence;

    ImageView imageViewProfile;

    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextFees;
    EditText editTextAddress;
    EditText editTextZipCode;
    EditText editTextEmail;
    EditText editTextPhoneNumber;
    EditText editTextLicenceNumber;

    EditText editTextExpiryDate;


    //Spinner spinnerProfession;

    KeyListener firstNameKeyListener;
    KeyListener lastNameKeyListener;
    KeyListener textFeesKeyListener;
    KeyListener addressKeyListener;
    KeyListener zipCodeKeyListener;
    KeyListener emailKeyListener;
    KeyListener phoneNumberKeyListener;
    KeyListener licenceNumberKeyListener;

    boolean isEditable = false;

    Button buttonEdit;

    private Calendar calendar;


    public Profile()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_profile, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        textViewRating = (TextView) view.findViewById(R.id.textViewRating);

        linearLayoutProvider = (LinearLayout) view.findViewById(R.id.linearLayoutProvider);
        linearLayoutLicence = (LinearLayout) view.findViewById(R.id.linearLayoutLicence);

        imageViewProfile = (ImageView) view.findViewById(R.id.imageViewProfile);

        editTextFirstName = (EditText) view.findViewById(R.id.editTextFirstName);
        editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        editTextFees = (EditText) view.findViewById(R.id.editTextFees);
        editTextAddress = (EditText) view.findViewById(R.id.editTextAddress);
        editTextZipCode = (EditText) view.findViewById(R.id.editTextZipCode);
        editTextEmail = (EditText) view.findViewById(R.id.editTextEmail);
        editTextPhoneNumber = (EditText) view.findViewById(R.id.editTextPhoneNumber);
        editTextLicenceNumber = (EditText) view.findViewById(R.id.editTextLicenceNumber);
        editTextExpiryDate = (EditText) view.findViewById(R.id.editTextExpiryDate);

        editTextExpiryDate.setKeyListener(null);
        editTextExpiryDate.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {

                if(motionEvent.getAction()== MotionEvent.ACTION_UP)
                {
                    openDatePicker();
                    return true;
                }
                return false;
            }
        });

        //spinnerProfession=(Spinner) view.findViewById(R.id.spinnerProfession);


        (buttonEdit = (Button) view.findViewById(R.id.buttonEdit)).setOnClickListener(this);

        view.findViewById(R.id.buttonChangePassword).setOnClickListener(this);

        firstNameKeyListener = editTextFirstName.getKeyListener();
        lastNameKeyListener = editTextLastName.getKeyListener();
        textFeesKeyListener = editTextFees.getKeyListener();
        addressKeyListener = editTextAddress.getKeyListener();
        zipCodeKeyListener = editTextZipCode.getKeyListener();
        emailKeyListener = editTextEmail.getKeyListener();
        phoneNumberKeyListener = editTextPhoneNumber.getKeyListener();
        licenceNumberKeyListener = editTextLicenceNumber.getKeyListener();

        disableAll();

    }

    public void disableAll()
    {
        editTextFirstName.setKeyListener(null);
        editTextLastName.setKeyListener(null);
        editTextFees.setKeyListener(null);
        editTextAddress.setKeyListener(null);
        editTextZipCode.setKeyListener(null);
        editTextEmail.setKeyListener(null);
        editTextPhoneNumber.setKeyListener(null);
        editTextLicenceNumber.setKeyListener(null);

        editTextExpiryDate.setEnabled(false);
        //  spinnerProfession.getSelectedView().setEnabled(false);
        //spinnerProfession.setEnabled(false);

    }

    public void enableAll()
    {
        editTextFirstName.setKeyListener(firstNameKeyListener);
        editTextLastName.setKeyListener(lastNameKeyListener);
        editTextFees.setKeyListener(textFeesKeyListener);
        editTextAddress.setKeyListener(addressKeyListener);
        editTextZipCode.setKeyListener(zipCodeKeyListener);
        editTextEmail.setKeyListener(emailKeyListener);
        editTextPhoneNumber.setKeyListener(phoneNumberKeyListener);
        editTextLicenceNumber.setKeyListener(licenceNumberKeyListener);

        editTextExpiryDate.setEnabled(true);


        //  spinnerProfession.getSelectedView().setEnabled(true);
        // spinnerProfession.setEnabled(true);
    }


    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity) getActivity()).textViewTitle.setText("Dr. Rob Williams");

        if (MyConstants.WHO.equals(MyConstants.PROVIDER))
        {
            ((MainActivity) getActivity()).textViewSubTitle.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).textViewSubTitle.setText("Cardiologist");
        }
        else
        {

            ((MainActivity) getActivity()).textViewTitle.setText("Williams");

            ratingBar.setVisibility(View.GONE);
            textViewRating.setText("44 Shirley Ave. West Chicago, IL 60185");
            textViewRating.setTextSize(15);
            textViewRating.setPadding(0, 0, 0, 10);
            linearLayoutProvider.setVisibility(View.GONE);
            linearLayoutLicence.setVisibility(View.GONE);

            imageViewProfile.setImageResource(R.drawable.user_5);
            imageViewProfile.getLayoutParams().height = 180;
            imageViewProfile.getLayoutParams().width = 180;
        }
    }


    public void openDatePicker()
    {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(context, this, year, month, dayOfMonth).show();
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day)
    {
        int actualMonth = month + 1;
        //Log.e(TAG,"Year "+year+" month "+month+1+" day "+day);

        String date = year + "-" + actualMonth + "-" + day;

        editTextExpiryDate.setText(CommonMethods.getInstance().parseDateToddMMyyyy2(date));


    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.buttonEdit:

                if (!isEditable)
                {
                    buttonEdit.setText("SUBMIT");

                    enableAll();

                    isEditable = true;


                }
                else
                {
                    buttonEdit.setText("EDIT");
                    disableAll();

                    isEditable = false;

                    CommonMethods.getInstance().hideKeyBoard(getActivity());

                }
                break;

            case R.id.buttonChangePassword:

                MyDialogs.getInstance().commonDialog(context, getActivity(), "Change Password", "Change", 2);

                break;


            /*case R.id.editTextExpiryDate:



                break;*/
        }
    }
}
