package ballidaku.calldoctor.mainScreens.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.commonScreens.SearchLocationActivity;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;


public class Search extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener
{

    View view = null;

    Context context;

    private Calendar calendar;

    EditText editTextDate;
    EditText editTextLocation;



    public Search()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_search, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        view.findViewById(R.id.buttonSearch).setOnClickListener(this);

        editTextLocation=(EditText)view.findViewById(R.id.editTextLocation);

        editTextDate=(EditText)view.findViewById(R.id.editTextDate);
        editTextDate.setKeyListener(null);


        editTextDate.setOnKeyListener(null);
        editTextDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextDate.getRight() - editTextDate.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        openDatePicker();
                        return true;
                    }
                }
                return false;
            }
        });


        editTextLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(context, SearchLocationActivity.class);
                startActivityForResult(intent,500);
            }
        });


    }


        @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity)getActivity()).textViewTitle.setText("Search for provider");
        ((MainActivity)getActivity()).textViewSubTitle.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.buttonSearch:

                ((MainActivity) getActivity()).changeFragment(7, true);

                break;
        }
    }


    public void openDatePicker()
    {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(context, this, year, month, dayOfMonth).show();
    }



    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day)
    {
        int actualMonth = month + 1;
        //Log.e(TAG,"Year "+year+" month "+month+1+" day "+day);

        String date = year + "-" + actualMonth + "-" + day;

        editTextDate.setText(date);


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 500) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result").trim();

                editTextLocation.setText(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
