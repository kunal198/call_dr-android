package ballidaku.calldoctor.mainScreens.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.adapters.MessagesAdapter;
import ballidaku.calldoctor.dataModels.MessageModel;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.RecyclerItemClickListener;

/**
 * Created by brst-pc93 on 7/12/17.
 */

public class Messages extends Fragment
{

    String TAG = Messages.class.getSimpleName();

    View view = null;
    RecyclerView recyclerViewMessages;

    MessagesAdapter messagesAdapter;

    ArrayList<MessageModel> messageModelArrayList;

    MessageModel messageModel;

    Context context;

    public Messages()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_messages, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {
        recyclerViewMessages = (RecyclerView) view.findViewById(R.id.recyclerViewMessages);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewMessages.getContext(), DividerItemDecoration.VERTICAL);
        recyclerViewMessages.addItemDecoration(dividerItemDecoration);


        messageModelArrayList = new ArrayList<>();

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Barry Devito");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_1);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Cassy Farrier");
        messageModel.setMessage("I am byusy today");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_2);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Miki Winget");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_4);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Navjot");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_3);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Cassy Farrier");
        messageModel.setMessage("I am byusy today");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_2);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Miki Winget");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_4);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Navjot");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_3);
        messageModelArrayList.add(messageModel);
        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Navjot");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_3);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Cassy Farrier");
        messageModel.setMessage("I am byusy today");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_2);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Miki Winget");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_4);
        messageModelArrayList.add(messageModel);

        messageModel = new MessageModel();
        messageModel.setUserName("Dr. Navjot");
        messageModel.setMessage("Hii..We have a appointment tomorrow");
        messageModel.setTime("10:39 am");
        messageModel.setImage(R.drawable.ic_user_3);
        messageModelArrayList.add(messageModel);


        messagesAdapter = new MessagesAdapter(messageModelArrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerViewMessages.setLayoutManager(mLayoutManager);
        recyclerViewMessages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMessages.setAdapter(messagesAdapter);


        recyclerViewMessages.addOnItemTouchListener(new RecyclerItemClickListener(context, recyclerViewMessages,
                  new RecyclerItemClickListener.OnItemClickListener()
                  {
                      @Override
                      public void onItemClick(View view, int position)
                      {
                          Log.e(TAG, "" + position);

                          // CommonMethods.getInstance().changeFragment(getActivity(),new Chat(),true);


                          ((MainActivity) getActivity()).changeFragment(6, true);
                      }

                      @Override
                      public void onLongItemClick(View view, int position)
                      {
                          // do whatever
                      }
                  }));

        recyclerViewMessages.addOnScrollListener(new HideShowScrollListener()
        {
            @Override
            public void onHide()
            {
                // ((MainActivity)getActivity()).linearLayoutTabs.animate().setInterpolator(new AccelerateDecelerateInterpolator()).scaleX(1).scaleY(1);
                // ((MainActivity)getActivity()).linearLayoutTabs.animate().translationY(-((MainActivity)getActivity()).linearLayoutTabs.getHeight()).setInterpolator(new AccelerateInterpolator(2));
                // do your hiding animation here

                //
                ((MainActivity)getActivity()).linearLayoutTabs.setVisibility(View.GONE);
              //  LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ((MainActivity) getActivity()).linearLayoutTabs.getLayoutParams();
              //  int fabBottomMargin = lp.bottomMargin;
               // ((MainActivity) getActivity()).linearLayoutTabs.animate().translationY(((MainActivity) getActivity()).linearLayoutTabs.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
               // ((MainActivity) getActivity()).linearLayoutTabs.animate().translationY(((MainActivity) getActivity()).linearLayoutTabs.getHeight() + fabBottomMargin).setDuration(500);
                //
            }


            @Override
            public void onShow()
            {
                /// ((MainActivity)getActivity()).linearLayoutTabs.animate().setInterpolator(new AccelerateDecelerateInterpolator()).scaleX(1).scaleY(1);
                // do your showing animation here

                //((MainActivity)getActivity()).linearLayoutTabs.animate().scaleY(1);
                // ((MainActivity)getActivity()).linearLayoutTabs.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));


                //
                //((MainActivity) getActivity()).linearLayoutTabs.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
                //((MainActivity) getActivity()).linearLayoutTabs.animate().translationY(0).setDuration(500);
                 ((MainActivity)getActivity()).linearLayoutTabs.setVisibility(View.VISIBLE);

            }
        });


    }


        @Override
        public void onResume ()
        {
            super.onResume();

            ((MainActivity) getActivity()).textViewTitle.setText("Messages");
            ((MainActivity) getActivity()).textViewSubTitle.setVisibility(View.GONE);
        }


        public abstract class HideShowScrollListener extends RecyclerView.OnScrollListener
        {
            private static final int HIDE_THRESHOLD = 20;
            private int scrolledDistance = 0;
            private boolean controlsVisible = true;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible)
                {
                    onHide();
                    controlsVisible = false;
                    scrolledDistance = 0;
                }
                else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible)
                {
                    onShow();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }

                if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0))
                {
                    scrolledDistance += dy;
                }
            }

            public abstract void onHide();

            public abstract void onShow();

        }


    }
