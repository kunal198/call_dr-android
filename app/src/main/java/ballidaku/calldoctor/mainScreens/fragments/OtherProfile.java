package ballidaku.calldoctor.mainScreens.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;

/**
 * Created by brst-pc93 on 7/18/17.
 */

public class OtherProfile  extends Fragment implements OnMapReadyCallback
{

    String TAG = OtherProfile.class.getSimpleName();

    Context context;

    View view = null;

    RatingBar ratingBar;
    TextView textViewRating;

    LinearLayout linearLayoutProvider;

    ImageView imageViewProfile;

    SupportMapFragment mapFragment;

    private GoogleMap mMap;


    public OtherProfile()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.fragment_other_profile, container, false);

            context = getActivity();

            setUpViews();
        }


        return view;
    }

    private void setUpViews()
    {

        mapFragment = (SupportMapFragment)this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ratingBar=(RatingBar)view.findViewById(R.id.ratingBar);
        textViewRating=(TextView) view.findViewById(R.id.textViewRating);

        linearLayoutProvider=(LinearLayout) view.findViewById(R.id.linearLayoutProvider);

        imageViewProfile=(ImageView) view.findViewById(R.id.imageViewProfile);
    }


    @Override
    public void onResume()
    {
        super.onResume();

        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);

        /*if(MyConstants.WHO.equals(MyConstants.PROVIDER))
        {*/
            ((MainActivity)getActivity()).textViewTitle.setText("Dr. Rob Williams");
            ((MainActivity) getActivity()).textViewSubTitle.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).textViewSubTitle.setText("Cardiologist");
       /* }
        else
        {

            ((MainActivity)getActivity()).textViewTitle.setText("Williams");

            ratingBar.setVisibility(View.GONE);
            textViewRating.setText("44 Shirley Ave. West Chicago, IL 60185");
            textViewRating.setTextSize(15);
            textViewRating.setPadding(0,0,0,10);
            linearLayoutProvider.setVisibility(View.GONE);

            imageViewProfile.setImageResource(R.drawable.user_5);
            imageViewProfile.getLayoutParams().height = 180;
            imageViewProfile.getLayoutParams().width = 180;
        }*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                //Location Permission already granted
               // buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
            else
            {
                //Request Location Permission
                checkLocationPermission();
            }
        }
        else
        {
            // buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                      Manifest.permission.ACCESS_FINE_LOCATION))
            {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                          .setTitle("Location Permission Needed")
                          .setMessage("This app needs the Location permission, please accept to use location functionality")
                          .setPositiveButton("OK", new DialogInterface.OnClickListener()
                          {
                              @Override
                              public void onClick(DialogInterface dialogInterface, int i)
                              {
                                  //Prompt the user once explanation has been shown
                                  ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                              }
                          })
                          .create()
                          .show();


            }
            else
            {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                          MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }







    @Override
    public void onDestroy()
    {

        super.onDestroy();

        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

    }



}
