package ballidaku.calldoctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.dataModels.MessageModel;

/**
 * Created by brst-pc93 on 7/12/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder>
{

    private List<MessageModel> messageModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textViewName, textViewMessage, textViewTime;
        ImageView imageViewOwner;

        public MyViewHolder(View view)
        {
            super(view);
            imageViewOwner = (ImageView) view.findViewById(R.id.imageViewOwner);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
            textViewTime = (TextView) view.findViewById(R.id.textViewTime);
        }
    }


    public MessagesAdapter(List<MessageModel> commentsList)
    {
        this.messageModelList = commentsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_messages_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        MessageModel messageModel = messageModelList.get(position);
        holder.imageViewOwner.setImageResource(messageModel.getImage());
        holder.textViewName.setText(messageModel.getUserName());
        holder.textViewMessage.setText(messageModel.getMessage());
        holder.textViewTime.setText(messageModel.getTime());
    }

    @Override
    public int getItemCount()
    {
        return messageModelList.size();
    }


    @Override
    public int getItemViewType(int position)
    {
        return super.getItemViewType(position);
    }
}