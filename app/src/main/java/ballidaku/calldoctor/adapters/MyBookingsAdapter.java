package ballidaku.calldoctor.adapters;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.dataModels.BookingsModel;
import ballidaku.calldoctor.mainScreens.activities.DetailsActivity;
import ballidaku.calldoctor.myUtilities.CommonMethods;
import ballidaku.calldoctor.myUtilities.MyConstants;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class MyBookingsAdapter extends RecyclerView.Adapter<MyBookingsAdapter.MyViewHolder> implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{

    private String TAG =MyBookingsAdapter.class.getSimpleName();

    private Context context;

    private Calendar calendar;


    private List<BookingsModel> bookingsModelList;

    int selectedposition=0;


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView textViewName;
        TextView textViewAddress;

        TextView textViewDate;

        TextView textViewDetails;
        TextView textViewCancel;
        TextView textViewAccept;
        TextView textViewAccepted;
        TextView textViewEdit;


        ImageView imageViewOwner;
        ImageView imageViewGreenTick;


        LinearLayout linearlayoutTypeRating;

        public MyViewHolder(View view)
        {
            super(view);
            imageViewOwner = (ImageView) view.findViewById(R.id.imageViewOwner);
            imageViewGreenTick = (ImageView) view.findViewById(R.id.imageViewGreenTick);

            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewAddress = (TextView) view.findViewById(R.id.textViewAddress);

            textViewDate = (TextView) view.findViewById(R.id.textViewDate);

            textViewDetails = (TextView) view.findViewById(R.id.textViewDetails);
            textViewCancel = (TextView) view.findViewById(R.id.textViewCancel);
            textViewAccept = (TextView) view.findViewById(R.id.textViewAccept);
            textViewAccepted = (TextView) view.findViewById(R.id.textViewAccepted);
            textViewEdit = (TextView) view.findViewById(R.id.textViewEdit);


            linearlayoutTypeRating = (LinearLayout) view.findViewById(R.id.linearlayoutTypeRating);

            textViewDetails.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Log.e(TAG,"Hello");
                    context.startActivity(new Intent(context, DetailsActivity.class));
                }
            });

            textViewAccept.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Log.e(TAG,"Hello");
                    context.startActivity(new Intent(context, DetailsActivity.class));
                }
            });

            textViewEdit.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    selectedposition=getAdapterPosition();
                    openDatePicker();
                }
            });


        }
    }


    public MyBookingsAdapter(Context context, List<BookingsModel> bookingsModelList)
    {
        this.context = context;
        this.bookingsModelList = bookingsModelList;
    }

    @Override
    public MyBookingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bookings_item, parent, false);

        return new MyBookingsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyBookingsAdapter.MyViewHolder holder, int position)
    {
        BookingsModel bookingsModel = bookingsModelList.get(position);

        holder.imageViewOwner.setImageResource(bookingsModel.getUserImage());
        holder.textViewName.setText(bookingsModel.getUserName());
        holder.textViewAddress.setText(bookingsModel.getUserAddress());
        holder.textViewDate.setText(bookingsModel.getDate());

        if (position == 0 && MyConstants.WHO.equals(MyConstants.PROVIDER))
        {
            holder.textViewCancel.setVisibility(View.GONE);
            holder.textViewAccept.setVisibility(View.GONE);


            holder.linearlayoutTypeRating.setVisibility(View.GONE);
            holder.textViewEdit.setVisibility(View.GONE);
            //holder.textViewDate.setVisibility(View.GONE);

        }
        else if (MyConstants.WHO.equals(MyConstants.PROVIDER))
        {
            holder.textViewAccepted.setVisibility(View.GONE);
            holder.imageViewGreenTick.setVisibility(View.GONE);

            holder.linearlayoutTypeRating.setVisibility(View.GONE);
            //holder.textViewDate.setVisibility(View.GONE);
        }
        else if (position == 0 && MyConstants.WHO.equals(MyConstants.PATIENT))
        {
            holder.imageViewGreenTick.setVisibility(View.GONE);

            holder.textViewDetails.setVisibility(View.GONE);
            holder.textViewAccept.setVisibility(View.GONE);
            holder.textViewAccepted.setVisibility(View.GONE);


            /*holder.linearlayoutTypeRating.setVisibility(View.GONE);
            holder.textViewEdit.setVisibility(View.GONE);
            holder.textViewDate.setVisibility(View.GONE);*/

        }
        else if (MyConstants.WHO.equals(MyConstants.PATIENT))
        {
            holder.imageViewGreenTick.setVisibility(View.GONE);

            holder.textViewDetails.setVisibility(View.GONE);
            holder.textViewAccept.setVisibility(View.GONE);
            holder.textViewAccepted.setVisibility(View.GONE);

           /* holder.linearlayoutTypeRating.setVisibility(View.GONE);
            holder.textViewDate.setVisibility(View.GONE);*/
        }


    }

    @Override
    public int getItemCount()
    {
        return bookingsModelList.size();
    }


    public void openTimePicker()
    {
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        new TimePickerDialog(context, this, hour, minute, false).show();
    }

    public void openDatePicker()
    {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(context, this, year, month, dayOfMonth).show();
    }


    String date = "";

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day)
    {
        int actualMonth = month + 1;
        //Log.e(TAG,"Year "+year+" month "+month+1+" day "+day);

        date = year + "-" + actualMonth + "-" + day;

        openTimePicker();
    }


    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1)
    {

        String time = i + ":" + i1;

        //Log.e(TAG, "DateTime---" + parseDateToddMMyyyy(date + " " + time));

        bookingsModelList.get(selectedposition).setDate(CommonMethods.getInstance().parseDateToddMMyyyy(date + " " + time));
        bookingsModelList.get(selectedposition).setStatus(MyConstants.REQUESTED);
        notifyDataSetChanged();

    }


}