package ballidaku.calldoctor.adapters;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.dataModels.SelectDoctorModel;
import ballidaku.calldoctor.myUtilities.CommonMethods;
import ballidaku.calldoctor.myUtilities.MyConstants;

/**
 * Created by brst-pc93 on 7/18/17.
 */

public class SelectDoctorAdapter extends RecyclerView.Adapter<SelectDoctorAdapter.MyViewHolder> implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{

    private String TAG = SelectDoctorAdapter.class.getSimpleName();

    private Context context;

    private Calendar calendar;

    private List<SelectDoctorModel> selectDoctorModelList;

    int selectedposition;

    OnItemTouchListener itemTouchListener;


    public interface OnItemTouchListener
    {
        public void onViewClick(View view, int position);

    }


    public SelectDoctorAdapter(Context context, List<SelectDoctorModel> bookingsModelList, OnItemTouchListener itemTouchListener)
    {
        this.context = context;
        this.selectDoctorModelList = bookingsModelList;
        this.itemTouchListener=itemTouchListener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView textViewName;
        TextView textViewAddress;

        TextView textViewBook;
        TextView textViewDate;

        ImageView imageViewOwner;

        LinearLayout linearlayoutTypeRating;
        LinearLayout linearLayoutItem;


        public MyViewHolder(View view)
        {
            super(view);
            imageViewOwner = (ImageView) view.findViewById(R.id.imageViewOwner);

            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewAddress = (TextView) view.findViewById(R.id.textViewAddress);

            textViewBook = (TextView) view.findViewById(R.id.textViewBook);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);

            linearlayoutTypeRating = (LinearLayout) view.findViewById(R.id.linearlayoutTypeRating);
            linearLayoutItem = (LinearLayout) view.findViewById(R.id.linearLayoutItem);

            textViewBook.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    selectedposition = getAdapterPosition();
                    openDatePicker();

                }
            });


            linearLayoutItem.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    itemTouchListener.onViewClick(view, getAdapterPosition());

                }
            });


        }
    }


    @Override
    public SelectDoctorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_select_doctor_item, parent, false);

        return new SelectDoctorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SelectDoctorAdapter.MyViewHolder holder, int position)
    {
        SelectDoctorModel selectDoctorModel = selectDoctorModelList.get(position);

        holder.imageViewOwner.setImageResource(selectDoctorModel.getUserImage());
        holder.textViewName.setText(selectDoctorModel.getUserName());
        holder.textViewAddress.setText(selectDoctorModel.getUserAddress());

        if (selectDoctorModel.getStatus().equals(MyConstants.BOOK))
        {
            holder.textViewBook.setText("BOOK");
            holder.textViewDate.setVisibility(View.GONE);
        }
        else if (selectDoctorModel.getStatus().equals(MyConstants.REQUESTED))
        {
            holder.textViewBook.setText("REQUESTED");
            holder.textViewDate.setText(selectDoctorModel.getDate());
            holder.textViewDate.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount()
    {
        return selectDoctorModelList.size();
    }


    private void openTimePicker()
    {
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        new TimePickerDialog(context, this, hour, minute, false).show();
    }

    private void openDatePicker()
    {
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(context, this, year, month, dayOfMonth).show();
    }


    String date = "";

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day)
    {
        int actualMonth = month + 1;
        //Log.e(TAG,"Year "+year+" month "+month+1+" day "+day);

        date = year + "-" + actualMonth + "-" + day;

        openTimePicker();
    }


    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1)
    {

        String time = i + ":" + i1;

        //Log.e(TAG, "DateTime---" + parseDateToddMMyyyy(date + " " + time));

        selectDoctorModelList.get(selectedposition).setDate(CommonMethods.getInstance().parseDateToddMMyyyy(date + " " + time));
        selectDoctorModelList.get(selectedposition).setStatus(MyConstants.REQUESTED);
        notifyDataSetChanged();

    }




}