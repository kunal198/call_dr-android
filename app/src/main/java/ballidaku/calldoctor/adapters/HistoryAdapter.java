package ballidaku.calldoctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.dataModels.HistoryModel;
import ballidaku.calldoctor.myUtilities.MyConstants;

/**
 * Created by brst-pc93 on 7/14/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

//    private static final int TYPE_HEADER = 0;
//    private static final int TYPE_ITEM = 1;

    private List<HistoryModel> historyModelList;

    public HistoryAdapter(List<HistoryModel> historyModelList)
    {
        this.historyModelList = historyModelList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_messages_item, parent, false);
//
//        return new HistoryAdapter.MyViewHolder(itemView);

        View itemView ;

       /* if (viewType == TYPE_ITEM)
        {*/
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_history_item, parent, false);
            return new HistoryAdapter.MyViewHolderItem(itemView);

        /*}
        else // if (viewType == TYPE_HEADER)sssssszz222
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_history_header, parent, false);
            return new HistoryAdapter.MyViewHolderHeader(itemView);
        }*/


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        HistoryModel historyModel = historyModelList.get(position);

        /*if (holder instanceof MyViewHolderItem)
        {*/
            ((MyViewHolderItem) holder).imageViewOwner.setImageResource(historyModel.getUserImage());
            ((MyViewHolderItem) holder).textViewName.setText(historyModel.getAppointMentWith());
            ((MyViewHolderItem) holder).textViewAddress.setText(historyModel.getAddress());
            ((MyViewHolderItem) holder).textViewDate.setText(historyModel.getDate());
       /* }
        else if (holder instanceof MyViewHolderHeader)
        {
            ((MyViewHolderHeader) holder).textViewHeader.setText(historyModel.getTitle());
        }*/


    }

    public class MyViewHolderItem extends RecyclerView.ViewHolder
    {

        LinearLayout linearLayoutRate;

        TextView textViewName;
        TextView textViewDoctorType;
        TextView textViewAddress;
        TextView textViewDate;

        ImageView imageViewOwner;

        public MyViewHolderItem(View view)
        {
            super(view);
            imageViewOwner = (ImageView) view.findViewById(R.id.imageViewOwner);

            linearLayoutRate = (LinearLayout) view.findViewById(R.id.linearLayoutRate);

            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewDoctorType = (TextView) view.findViewById(R.id.textViewDoctorType);
            textViewAddress = (TextView) view.findViewById(R.id.textViewAddress);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);


            if(MyConstants.WHO.equals(MyConstants.PROVIDER))
            {
                textViewDoctorType.setVisibility(View.GONE);
                linearLayoutRate.setVisibility(View.GONE);
            }
        }
    }

   /* public class MyViewHolderHeader extends RecyclerView.ViewHolder
    {
        public TextView textViewHeader;

        public MyViewHolderHeader(View view)
        {
            super(view);
            textViewHeader = (TextView) view.findViewById(R.id.textViewHeader);
        }
    }*/

    @Override
    public int getItemCount()
    {
        return historyModelList.size();
    }


    @Override
    public int getItemViewType(int position)
    {
      /*  if (isPositionHeader(position))
        {
            return TYPE_HEADER;
        }
        else
        {
            return TYPE_ITEM;
        }*/
        return super.getItemViewType(position);
    }

    private boolean isPositionHeader(int position)
    {
        return historyModelList.get(position).getType().equals(MyConstants.HEADER);
    }

}