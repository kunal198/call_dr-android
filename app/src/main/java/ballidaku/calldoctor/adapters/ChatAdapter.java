package ballidaku.calldoctor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.dataModels.ChatModel;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder>
{

    private List<ChatModel> chatModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout linearLayoutOther;
        LinearLayout linearLayoutMy;


        TextView textViewOtherMessage;
        TextView textViewOtherTime;

        TextView textViewMyMessage;
        TextView textViewMyTime;

        ImageView imageViewOther;
        ImageView imageViewMe;

        public MyViewHolder(View view)
        {
            super(view);

            linearLayoutOther=(LinearLayout)view.findViewById(R.id.linearLayoutOther);
            linearLayoutMy=(LinearLayout)view.findViewById(R.id.linearLayoutMy);

            imageViewOther = (ImageView) view.findViewById(R.id.imageViewOther);
            imageViewMe = (ImageView) view.findViewById(R.id.imageViewMe);

            textViewOtherMessage = (TextView) view.findViewById(R.id.textViewOtherMessage);
            textViewOtherTime = (TextView) view.findViewById(R.id.textViewOtherTime);

            textViewMyMessage = (TextView) view.findViewById(R.id.textViewMyMessage);
            textViewMyTime = (TextView) view.findViewById(R.id.textViewMyTime);
        }
    }


    public ChatAdapter(List<ChatModel> chatModelList)
    {
        this.chatModelList = chatModelList;
    }

    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_chat_item, parent, false);

        return new ChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.MyViewHolder holder, int position)
    {
        ChatModel chatModel = chatModelList.get(position);

        if(position == 0)
        {
            holder.linearLayoutMy.setVisibility(View.GONE);

            holder.imageViewOther.setImageResource(chatModel.getImage());
            holder.textViewOtherMessage.setText(chatModel.getMessage());
            holder.textViewOtherTime.setText(chatModel.getTime());

        }
        else if(position == 1)
        {
            holder.linearLayoutOther.setVisibility(View.GONE);

            holder.imageViewMe.setImageResource(chatModel.getImage());
            holder.textViewMyMessage.setText(chatModel.getMessage());
            holder.textViewMyTime.setText(chatModel.getTime());

        }
    }

    @Override
    public int getItemCount()
    {
        return chatModelList.size();
    }
}