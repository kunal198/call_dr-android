package ballidaku.calldoctor.frontScreens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.commonScreens.SearchLocationActivity;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.MyConstants;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener
{
    Context context;

    TextView textViewPatient;
    TextView textViewDoctor;

    EditText editTextAddress;


    CheckBox checkBoxTerms;

    LinearLayout linearLayoutProvider;

    String ProviderPatient = MyConstants.PATIENT;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        context = this;

        setUpViews();
    }

    private void setUpViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);

        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        TextView textViewSubTitle = (TextView) findViewById(R.id.textViewSubTitle);
        textViewTitle.setText("Sign Up");

        textViewSubTitle.setVisibility(View.GONE);


        editTextAddress = (EditText) findViewById(R.id.editTextAddress);

        textViewPatient = (TextView) findViewById(R.id.textViewPatient);
        textViewDoctor = (TextView) findViewById(R.id.textViewDoctor);


        checkBoxTerms=(CheckBox)findViewById(R.id.checkBoxTerms);

        String checkBoxText = "I agree to all the <a href='http://www.redbus.in/mob/mTerms.aspx' > Terms and Conditions</a>";
        checkBoxTerms.setText(Html.fromHtml(checkBoxText));
        checkBoxTerms.setMovementMethod(LinkMovementMethod.getInstance());

        linearLayoutProvider = (LinearLayout) findViewById(R.id.linearLayoutProvider);

        textViewPatient.setOnClickListener(this);
        textViewDoctor.setOnClickListener(this);


        findViewById(R.id.buttonSignUp).setOnClickListener(this);

        changeView(MyConstants.PATIENT);



        editTextAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextAddress.getRight() - editTextAddress.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here


                        Intent intent=new Intent(context, SearchLocationActivity.class);
                        startActivityForResult(intent,500);

                        return true;
                    }
                }
                return false;
            }
        });


    }

    @Override
    public void onClick(View view)
    {

        switch (view.getId())
        {
            case R.id.textViewPatient:

                ProviderPatient = MyConstants.PATIENT;
                changeView(MyConstants.PATIENT);

                break;

            case R.id.textViewDoctor:

                ProviderPatient = MyConstants.PROVIDER;
                changeView(MyConstants.PROVIDER);

                break;


            case R.id.buttonSignUp:


                MyConstants.WHO = ProviderPatient;
                Intent intent = new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                break;

        }
    }


    public void changeView(String who)
    {


        switch (who)
        {
            case MyConstants.PATIENT:

                textViewPatient.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewPatient.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));

                textViewDoctor.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
                textViewDoctor.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));

                linearLayoutProvider.setVisibility(View.GONE);

                break;

            case MyConstants.PROVIDER:

                textViewDoctor.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                textViewDoctor.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));

                textViewPatient.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
                textViewPatient.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));

                linearLayoutProvider.setVisibility(View.VISIBLE);

                break;


        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 500) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");

                editTextAddress.setText(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
