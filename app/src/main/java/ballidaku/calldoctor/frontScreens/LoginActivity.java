package ballidaku.calldoctor.frontScreens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ballidaku.calldoctor.R;
import ballidaku.calldoctor.mainScreens.activities.MainActivity;
import ballidaku.calldoctor.myUtilities.MyDialogs;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context =this;

        setUpView();
    }

    private void setUpView()
    {
        findViewById(R.id.buttonSignIn).setOnClickListener(this);
        findViewById(R.id.textViewSignUp).setOnClickListener(this);
        findViewById(R.id.textViewForgotPassword).setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.buttonSignIn:

                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.textViewSignUp:

                Intent intent1 = new Intent(context, SignUpActivity.class);
                startActivity(intent1);

                break;

            case R.id.textViewForgotPassword:

                MyDialogs.getInstance().commonDialog(context,this,"Forgot Password","Send", 1);

                break;
        }
    }
}
