package ballidaku.calldoctor.frontScreens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import ballidaku.calldoctor.R;

public class SplashActivity extends AppCompatActivity
{

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        context = this;


        // this is to check whether app is in background or not, if yes finish the new one
        if (!isTaskRoot()
                  && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                  && getIntent().getAction() != null
                  && getIntent().getAction().equals(Intent.ACTION_MAIN))
        {

            finish();
            return;
        }


        GoToNextScreen.start();
    }
    CountDownTimer GoToNextScreen =new CountDownTimer(1000,1000)
    {
        @Override
        public void onTick(long l)
        {

        }

        @Override
        public void onFinish()
        {
            Intent intent = new Intent(context, LoginActivity.class);
           //Intent intent = new Intent(context, Details.class);
//            Intent intent = new Intent(context, MainActivity.class);
            //Intent intent = new Intent(context, ProfileActivity.class);
            //Intent intent = new Intent(context, AddJokeActivity.class);

            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

            finish();
        }
    };




    @Override
    protected void onDestroy()
    {
        GoToNextScreen.cancel();

        //Log.e("onDestroy","onDestroy");
        super.onDestroy();


    }


}
