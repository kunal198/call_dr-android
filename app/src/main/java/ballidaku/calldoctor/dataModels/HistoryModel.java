package ballidaku.calldoctor.dataModels;

/**
 * Created by brst-pc93 on 7/14/17.
 */

public class HistoryModel
{

    int userImage;
    String appointMentWith;
    String doctorType;
    String address;
    String date;
    String type;
    String title;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }


    public int getUserImage()
    {
        return userImage;
    }

    public void setUserImage(int userImage)
    {
        this.userImage = userImage;
    }

    public String getAppointMentWith()
    {
        return appointMentWith;
    }

    public void setAppointMentWith(String appointMentWith)
    {
        this.appointMentWith = appointMentWith;
    }

    public String getDoctorType()
    {
        return doctorType;
    }

    public void setDoctorType(String doctorType)
    {
        this.doctorType = doctorType;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
}
