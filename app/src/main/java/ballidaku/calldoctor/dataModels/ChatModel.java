package ballidaku.calldoctor.dataModels;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class ChatModel
{
    String message;
    String time;
    String id;
    int image;


    public int getImage()
    {
        return image;
    }

    public void setImage(int image)
    {
        this.image = image;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}
