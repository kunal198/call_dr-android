package ballidaku.calldoctor.dataModels;

/**
 * Created by brst-pc93 on 7/13/17.
 */

public class BookingsModel
{
    String userName;
    int userImage;
    String userAddress;

    String status;
    String date;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public int getUserImage()
    {
        return userImage;
    }

    public void setUserImage(int userImage)
    {
        this.userImage = userImage;
    }

    public String getUserAddress()
    {
        return userAddress;
    }

    public void setUserAddress(String userAddress)
    {
        this.userAddress = userAddress;
    }
}
